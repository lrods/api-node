const { sequelize } = require('../startup/connection')
const { Sequelize, DataTypes } = require('sequelize')
const Joi = require('@hapi/joi')

const Customer = sequelize.define('Customer', {
    firstName: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    lastName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    isGold: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    telephoneNumber: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            len: [10, 50]
        }
    },
    email: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
        validate: {
            isEmail: true,
            len: [5, 1024]
        }
    },
    address: DataTypes.STRING
}, { tableName: 'Customers', paranoid: true })

Customer.sync()

function validateCustomer(customer) {
    const schema = Joi.object({
        firstName: Joi.string().min(3).max(50).required(),
        lastName: Joi.string().min(3).max(50).required(),
        isGold: Joi.boolean().required(),
        telephoneNumber: Joi.string().min(10).max(50).required(),
        email: Joi.string().email().min(5).max(255).required(),
        address: Joi.string().allow("", null)
    });

    return schema.validate(customer)
}

module.exports.Customer = Customer
module.exports.validate = validateCustomer