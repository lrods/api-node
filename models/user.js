const { sequelize } = require('../startup/connection')
const { Sequelize, DataTypes } = require('sequelize')
const Joi = require('@hapi/joi')
const jwt = require('jsonwebtoken')
const config = require('config')

const User = sequelize.define('User', {
    firstName: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            len: [2, 50]
        }
    },
    lastName: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            len: [2, 50]
        }
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
            isEmail: true,
            len: [5, 255]
        }
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            len: [5, 1024]
        }
    },
    isAdmin: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    }
}, { paranoid: true, tableName: 'Users' })

User.prototype.generateToken = function () {
    const token = jwt.sign({
        id: this.id,
        firstName: this.firstName,
        lastName: this.lastName,
        email: this.email,
        isAdmin: this.isAdmin
    }, config.get('jwtPrivateKey'))

    return token
}

User.sync()

function validateUser(user) {
    const schema = Joi.object({
        firstName: Joi.string().min(2).max(50).required(),
        lastName: Joi.string().min(2).max(50).required(),
        email: Joi.string().min(5).max(255).email().required(),
        password: Joi.string().min(5).max(255).required()
    })

    return schema.validate(user)
}

module.exports.User = User
module.exports.validate = validateUser