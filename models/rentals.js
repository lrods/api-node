const { sequelize } = require('../startup/connection')
const { Sequelize, DataTypes } = require('sequelize')
const Joi = require('@hapi/joi')

const Rental = sequelize.define('Rental', {
    dateOut: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW
    },
    dateReturned: {
        type: DataTypes.DATE
    },
    rentalFee: {
        type: DataTypes.DOUBLE,
        defaultValue: 0,
        validate: {
            min: 0
        }
    }
}, { tableName: 'Rentals', paranoid: true })

function validateRental(rental) {
    const schema = Joi.object({
        customerId: Joi.number().required(),
        movieId: Joi.number().required()
    })

    return schema.validate(rental)
}

module.exports.Rental = Rental
module.exports.validate = validateRental