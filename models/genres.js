const { sequelize } = require('../startup/connection')
const { Sequelize, DataTypes } = require('sequelize')
const Joi = require('@hapi/joi')

const Genre = sequelize.define('Genre', {
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            isAlpha: true,
            len: [3, 30]
        }
    }

}, { tableName: 'Genres' })

Genre.sync()

function validateGenre(genre) {
    const schema = Joi.object({
        name: Joi.string().min(3).max(30).required()
    })

    return schema.validate(genre)
}

module.exports.Genre = Genre
module.exports.validate = validateGenre