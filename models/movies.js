const { sequelize } = require('../startup/connection')
const { Sequelize, DataTypes } = require('sequelize')
const Joi = require('@hapi/joi')

const Movie = sequelize.define('Movie', {
    title: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    dailyRentalRate: {
        type: DataTypes.DOUBLE,
        defaultValue: 0
    },
    numberInStock: {
        type: DataTypes.TINYINT,
        defaultValue: 0
    }
}, { tableName: 'Movies' })

Movie.sync()

function validateMovie(movie) {

    const schema = Joi.object({
        title: Joi.string().required(),
        genre: Joi.array().min(1).required(),
        dailyRentalRate: Joi.number().required(),
        numberInStock: Joi.number().required(),
    });

    return schema.validate(movie)
}

module.exports.Movie = Movie
module.exports.validate = validateMovie