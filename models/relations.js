const { sequelize } = require('../startup/connection')
const { Sequelize, DataTypes } = require('sequelize')
const { Genre } = require('./genres')
const { Movie } = require('./movies')
const { Customer } = require('./customers')
const { Rental } = require('./rentals')

const MovieGenres = sequelize.define('moviegenres', {
    genreId: {
        type: DataTypes.INTEGER,
        references: {
            model: Genre,
            key: 'id'
        }
    },
    movieId: {
        type: DataTypes.INTEGER,
        references: {
            model: Movie,
            key: 'id'
        }
    }
}, { timestamps: true });

Genre.belongsToMany(Movie, { through: 'MovieGenres', onDelete: 'RESTRICT', onUpdate: 'CASCADE' })
Movie.belongsToMany(Genre, { through: 'MovieGenres', onDelete: 'NO ACTION', onUpdate: 'NO ACTION' })

MovieGenres.sync()

Movie.hasMany(Rental, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' })
Rental.belongsTo(Movie)

Customer.hasMany(Rental, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' })
Rental.belongsTo(Customer)

Rental.sync({})
