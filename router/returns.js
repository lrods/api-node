const express = require('express')
const router = express.Router()
const { sequelize } = require('../startup/connection')
const { Rental } = require('../models/rentals')
const { Movie } = require('../models/movies')
const auth = require('../middleware/auth')
const admin = require('../middleware/admin')
const Joi = require('@hapi/joi')
const moment = require('moment')


router.post('/', auth, async (req, res) => {
    const { error } = validate(req.body)

    if (error) return res.status(400).send(error.details[0].message)

    const rental = await Rental.findByPk(req.body.rentalId)
    if (!rental) return res.status(400).send('Rental not found.')

    if (rental.dateReturned) return res.status(400).send("Rental already processed.")

    movie = await Movie.findByPk(rental.MovieId)

    let rentalDays = moment().diff(rental.dateOut, 'days')
    rentalDays = (rentalDays === 0 ? 1 : rentalDays)
    const rentalFee = rentalDays * movie.dailyRentalRate

    try {
        const result = sequelize.transaction(async (t) => {
            rental.dateReturned = new Date()
            rental.rentalFee = rentalFee
            await rental.save({ transaction: t })

            movie.numberInStock = movie.numberInStock + 1
            await movie.save({ transaction: t })

            res.send(rental)
        })
    } catch (error) {
        res.status(500).send('Something failed.')
    }
})

router.delete('/:id', (req, res) => { })

function validate(req) {
    const schema = Joi.object({
        rentalId: Joi.number().required()
    })

    return schema.validate(req)
}

module.exports = router