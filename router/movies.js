const express = require('express')
const router = express.Router()
const { Movie, validate } = require('../models/movies')
const { sequelize } = require('../startup/connection')
const { Genre } = require('../models/genres')
const Joi = require('@hapi/joi')
const auth = require('../middleware/auth')
const admin = require('../middleware/admin')

router.get('/', async (req, res) => {
    allMovies = await Movie.findAll({ include: { model: Genre, attributes: ['id', 'name'], through: { attributes: [] } } })
    res.send(allMovies)
})

router.get('/:id', async (req, res) => {
    movie = await Movie.findByPk(req.params.id)

    if (movie === null) return res.status(404).send('The movie with the given ID was not found')

    res.send(movie)
})

router.post('/', auth, async (req, res) => {
    const { error } = validate(req.body)

    if (error) return res.status(400).send(error.details[0].message)

    try {
        const result = await sequelize.transaction(async (t) => {
            const movie = await Movie.create({
                title: req.body.title,
                dailyRentalRate: req.body.dailyRentalRate,
                numberInStock: req.body.numberInStock,
            }, { transaction: t })

            await movie.setGenres(req.body.genre, { transaction: t })

            res.send(movie)
        })
    } catch (error) {
        res.status(500).send(error)
    }
})

router.put('/:id', [auth], async (req, res) => {
    const schema = Joi.object({
        title: Joi.string().required(),
        dailyRentalRate: Joi.number().required(),
        numberInStock: Joi.number().required(),
    });

    const { error } = schema.validate(req.body)

    if (error) return res.status(400).send(error.details[0].message)

    movie = await Movie.findByPk(req.params.id)

    if (movie === null) return res.status(404).send('The movie with the given ID was not found')

    movie.dailyRentalRate = req.body.dailyRentalRate
    movie.numberInStock = req.body.numberInStock
    await movie.save()
    res.send(movie)
})

router.delete('/:id', (req, res) => { })

module.exports = router