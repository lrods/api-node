const express = require('express')
const router = express.Router()
const { User } = require('../models/user')
const bcrypt = require('bcrypt')
const Joi = require('@hapi/joi')

router.post('/', async (req, res) => {
    const { error } = validate(req.body)
    if (error) return res.status(400).send(error.details[0].message)

    let user = await User.findOne({ where: { email: req.body.email } })
    if (!user) res.status(400).send('Invalid email or password')

    const result = await bcrypt.compare(req.body.password, user.password)
    if (!result) res.status(400).send('Invalid email or password')

    const token = user.generateToken()
    res.send(token)
})

function validate(data) {
    const schema = Joi.object({
        email: Joi.string().min(5).max(255).email().required(),
        password: Joi.string().min(5).max(255).required()
    })

    return schema.validate(data)
}


module.exports = router