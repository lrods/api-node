const express = require('express')
const router = express.Router()
const { Genre, validate } = require('../models/genres')
const auth = require('../middleware/auth')
const admin = require('../middleware/admin')

router.get('/', async (req, res) => {
    allGenres = await Genre.findAll()
    res.send(allGenres)
})

router.get('/:id', async (req, res) => {
    genre = await Genre.findByPk(req.params.id)

    if (genre === null) return res.status(404).send('The genre with the given ID was not found')

    res.send(genre)
})

router.post('/', auth, async (req, res) => {
    const { error } = validate(req.body)

    if (error) return res.status(400).send(error.details[0].message)

    let genre = await Genre.create({ name: req.body.name })
    res.send(genre)
})

router.put('/:id', [auth], async (req, res) => {
    const { error } = validate(req.body)

    if (error) return res.status(400).send(error.details[0].message)

    genre = await Genre.findByPk(req.params.id)

    if (genre === null) return res.status(404).send('The genre with the given ID was not found')

    genre.name = req.body.name
    await genre.save()
    res.send(genre)
})

router.delete('/:id', (req, res) => { })

module.exports = router