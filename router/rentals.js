const express = require('express')
const router = express.Router()
const { sequelize } = require('../startup/connection')
const { Rental, validate } = require('../models/rentals')
const { Movie } = require('../models/movies')
const { Customer } = require('../models/customers')
const auth = require('../middleware/auth')
const admin = require('../middleware/admin')

router.get('/', async (req, res) => {
    allRentals = await Rental.findAll()
    res.send(allRentals)
})

router.get('/:id', async (req, res) => {
    rental = await Rental.findByPk(req.params.id)

    if (rental === null) return res.status(404).send('The rental with the given ID was not found')

    res.send(rental)
})

router.post('/', auth, async (req, res) => {
    const { error } = validate(req.body)

    if (error) return res.status(400).send(error.details[0].message)

    const customer = await Customer.findByPk(req.body.customerId)
    if (!customer) return res.status(400).send('Invalid customer!')

    const movie = await Movie.findByPk(req.body.movieId)
    if (!movie) return res.status(400).send('Invalid movie!')

    if (movie.numberInStock === 0) res.status(400).send('Movie not in stock.')

    try {
        const result = await sequelize.transaction(async (t) => {
            let rental = await Rental.create({
                CustomerId: req.body.customerId,
                MovieId: req.body.movieId
            }, { transaction: t })

            movie.numberInStock = movie.numberInStock - 1
            await movie.save({ transaction: t })

            res.send(rental)
        })
    } catch (error) {
        res.status(500).send(error)
    }
})

router.delete('/:id', (req, res) => { })

module.exports = router