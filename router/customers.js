const express = require('express')
const router = express.Router()
const { Customer, validate } = require('../models/customers')
const Joi = require('@hapi/joi')
const auth = require('../middleware/auth')
const admin = require('../middleware/admin')

router.get('/', async (req, res) => {
    allCustomers = await Customer.findAll()
    res.send(allCustomers)
})

router.get('/:id', async (req, res) => {
    customer = await Customer.findByPk(req.params.id)

    if (customer === null) return res.status(404).send('The customer with the given ID was not found')

    res.send(customer)
})

router.post('/', auth, async (req, res) => {
    const { error } = validate(req.body)

    if (error) return res.status(400).send(error.details[0].message)

    let customer = await Customer.findOne({ where: { email: req.body.email } })

    if (customer) return res.status(400).send('Customer already registered!')

    customer = await Customer.create({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        isGold: req.body.isGold,
        telephoneNumber: req.body.telephoneNumber,
        email: req.body.email,
        address: req.body.address
    })

    res.send(customer)
})

router.put('/:id', [auth, admin], async (req, res) => {
    const schema = Joi.object({
        isGold: Joi.boolean().required(),
        telephoneNumber: Joi.string().min(10).max(50).required(),
        email: Joi.string().email().min(5).max(255).required(),
        address: Joi.string().allow("", null)
    });

    const { error } = schema.validate(req.body)

    if (error) return res.status(400).send(error.details[0].message)

    let customer = await Customer.findByPk(req.params.id)

    if (customer === null) return res.status(404).send('The customer with the given ID was not found')

    customer.isGold = req.body.isGold
    customer.telephoneNumber = req.body.telephoneNumber
    customer.address = req.body.address
    await customer.save()
    res.send(customer)
})

router.delete('/:id', (req, res) => { })

module.exports = router