const express = require('express')
const router = express.Router()
const { User, validate } = require('../models/user')
const _ = require('lodash')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const auth = require('../middleware/auth')

router.get('/me', auth, async (req, res) => {
    let user = await User.findByPk(req.user.id)
    user = _.pick(user, ["id", "firstName", "lastName", "email", "isAdmin"])
    res.send(user)
})

router.post('/', async (req, res) => {
    const { error } = validate(req.body)

    if (error) return res.status(400).send(error.details[0].message)

    let user = await User.findOne({ where: { email: req.body.email } })

    if (user) return res.status(400).send('User already registered!')

    let password = req.body.password
    const salt = await bcrypt.genSalt(10)
    password = await bcrypt.hash(password, salt)

    user = await User.create({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: password,
        isAdmin: req.body.isAdmin
    })

    const token = user.generateToken()

    res.header("x-auth-token", token).send(_.pick(user, ['firstName', 'lastName', 'email']))

})


module.exports = router