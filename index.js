const express = require('express')
const app = express()
const { connect } = require('./startup/connection')
const Sentry = require('@sentry/node')

require('./startup/logging')()
app.use(Sentry.Handlers.requestHandler())
require('./startup/config')()
require('./startup/routes')(app)
app.use(Sentry.Handlers.errorHandler())
connect()

port = process.env.port || 3000

const server = app.listen(port, () => {
    Sentry.captureMessage(`App has started and is listening on port: ${port}`)
})

module.exports = server

