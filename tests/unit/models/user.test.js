require('mysql2/node_modules/iconv-lite').encodingExists('foo');

const { User } = require('../../../models/user')
const jwt = require('jsonwebtoken')
const config = require('config')

describe('user.generateAuthToken', () => {
    test('should return a valid JWT', () => {
        const data = { id: 1, isAdmin: true }
        const user = new User(data)
        const token = user.generateToken()
        const decoded = jwt.verify(token, config.get('jwtPrivateKey'))
        expect(decoded).toMatchObject(data)

    })
})