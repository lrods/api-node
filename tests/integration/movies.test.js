const request = require('supertest')
const { Genre } = require('../../models/genres')
const { Movie } = require('../../models/movies')
const { User } = require('../../models/user')
require('mysql2/node_modules/iconv-lite').encodingExists('foo');

let server;
let genre;

describe('/api/movies', () => {

    beforeEach(async () => {
        server = require('../../index')
        genre = await Genre.create({ name: 'Triller' })
    })

    afterEach(async () => {
        await server.close()
        await Genre.destroy({
            truncate: { cascade: true }
        })
        await Movie.destroy({
            truncate: { cascade: true }
        })
    })

    describe('/GET', () => {
        test('it should return all movies', async () => {
            const movie1 = await Movie.create({ title: 'Desperado', dailyRentalRate: 1.5, numberInStock: 15, genre: [genre.id] })
            const movie2 = await Movie.create({ title: 'Original sin', dailyRentalRate: 2.5, numberInStock: 20, genre: [genre.id] })

            const res = await request(server).get('/api/movies')
            expect(res.status).toBe(200)
            expect(res.body.length).toBe(2)
        })
    })

    describe('/GET/:id', () => {
        test('it should return a movie if a valid id is given', async () => {
            const movie = await Movie.create({ title: 'Desperado', dailyRentalRate: 1.5, numberInStock: 15, genre: [genre.id] })

            res = await request(server).get('/api/movies/' + movie.id)
            expect(res.status).toBe(200)
            expect(res.body).toHaveProperty('title', 'Desperado')
            expect(res.body).toHaveProperty('dailyRentalRate', 1.5)
            expect(res.body).toHaveProperty('numberInStock', 15)
        })

        test('it should return 404 if an invalid id is given', async () => {
            const movie = await Movie.create({ title: 'Desperado', dailyRentalRate: 1.5, numberInStock: 15, genre: [genre.id] })

            res = await request(server).get('/api/movies/4')
            expect(res.status).toBe(404)
        })
    })

    describe('POST/', () => {
        let token;
        let data;

        const exec = async () => {
            return await request(server).post('/api/movies').set('x-auth-token', token).send({ title, genre, dailyRentalRate, numberInStock })
        }

        beforeEach(async () => {
            token = new User().generateToken()
            title = "Desperado"
            genre = [genre.id]
            dailyRentalRate = 1.5
            numberInStock = 15

        })

        test('it should return 401 if no token is provided', async () => {
            token = ''
            res = await exec()
            expect(res.status).toBe(401)
        })

        test('it should return 400 if an invalid token is provided', async () => {
            token = 'token'
            res = await exec()
            expect(res.status).toBe(400)
        })

        test('it should return 400 if no title is provided', async () => {
            title = ""
            res = await exec()
            expect(res.status).toBe(400)
        })

        test('it should return 400 if no genre is provided', async () => {
            genre = ""
            res = await exec()
            expect(res.status).toBe(400)
        })
    })

    describe('PUT/:id', () => {
        let token;
        let title;
        let dailyRentalRate;
        let numberInStock;
        let movie;
        let id;

        const exec = async () => {
            return await request(server).put('/api/movies/' + id).set('x-auth-token', token).send({ title, dailyRentalRate, numberInStock })
        }

        beforeEach(async () => {
            movie = await Movie.create({ title: 'Desperado', dailyRentalRate: 1.5, numberInStock: 15 })
            id = movie.id
            title = movie.title
            dailyRentalRate = 2.5
            numberInStock = movie.numberInStock
            token = new User().generateToken()
        })

        test('it should return 401 if no token is provided', async () => {
            token = ''
            res = await exec()
            expect(res.status).toBe(401)
        })

        test('it should return 400 if an invalid token is provided', async () => {
            token = 'token'
            res = await exec()
            expect(res.status).toBe(400)
        })

        test('it should return 404 if an invalid id is provided', async () => {
            id = 20
            res = await exec()
            expect(res.status).toBe(404)
        })

        test('it should return 400 if no genre is provided', async () => {
            title = ''
            res = await exec()
            expect(res.status).toBe(400)
        })

        test('it should save if everything is valid', async () => {
            res = await exec()
            expect(res.status).toBe(200)
            expect(res.body).toHaveProperty('id')
            expect(res.body).toHaveProperty('dailyRentalRate', 2.5)
        })
    })

})