const request = require('supertest')
const { Genre } = require('../../models/genres')
const { User } = require('../../models/user')
require('mysql2/node_modules/iconv-lite').encodingExists('foo');

let server;

describe('/api/genres', () => {

    beforeEach(async () => {
        server = require('../../index')
    })

    afterEach(async () => {
        await server.close()
        await Genre.destroy({
            truncate: { cascade: true }
        })
    })

    describe('/GET', () => {
        test('it should return all genres', async () => {
            const genre1 = await Genre.create({ name: 'Thriller' })
            const genre2 = await Genre.create({ name: 'Comedy' })

            const res = await request(server).get('/api/genres')
            expect(res.status).toBe(200)
            expect(res.body.length).toBe(2)
            expect(res.body.some(g => g.name === 'Thriller')).toBeTruthy()
            expect(res.body.some(g => g.name === 'Comedy')).toBeTruthy()
        })
    })

    describe('/GET/:id', () => {
        test('it should return a genre if a valid id is given', async () => {
            genre = await Genre.create({ name: 'Thriller' })

            res = await request(server).get('/api/genres/' + genre.id)
            expect(res.status).toBe(200)
            expect(res.body).toHaveProperty('name', 'Thriller')
        })

        test('it should return 404 if an invalid id is given', async () => {
            genre = await Genre.create({ name: 'Thriller' })

            res = await request(server).get('/api/genres/4')
            expect(res.status).toBe(404)
        })
    })

    describe('POST/', () => {
        let token;
        let name;

        const exec = async () => {
            return await request(server).post('/api/genres').set('x-auth-token', token).send({ name })
        }

        beforeEach(async () => {
            token = new User().generateToken()
            name = 'Action'
        })

        test('it should return 401 if no token is provided', async () => {
            token = ''
            res = await exec()
            expect(res.status).toBe(401)
        })

        test('it should return 400 if an invalid token is provided', async () => {
            token = 'token'
            res = await exec()
            expect(res.status).toBe(400)
        })

        test('it should return 400 if no genre is provided', async () => {
            name = ''
            res = await exec()
            expect(res.status).toBe(400)
        })

        test('it should save if everything is valid', async () => {
            res = await exec()
            expect(res.body).toHaveProperty('id')
            expect(res.body).toHaveProperty('name', 'Action')
        })
    })

    describe('PUT/:id', () => {
        let token;
        let name;
        let genre;
        let id;

        const exec = async () => {
            return await request(server).put('/api/genres/' + id).set('x-auth-token', token).send({ name })
        }

        beforeEach(async () => {
            genre = await Genre.create({ name: 'Action' })
            id = genre.id
            token = new User().generateToken()
            name = 'Romantic'
        })

        test('it should return 401 if no token is provided', async () => {
            token = ''
            res = await exec()
            expect(res.status).toBe(401)
        })

        test('it should return 400 if an invalid token is provided', async () => {
            token = 'token'
            res = await exec()
            expect(res.status).toBe(400)
        })

        test('it should return 404 if an invalid id is provided', async () => {
            id = 20
            res = await exec()
            expect(res.status).toBe(404)
        })

        test('it should return 400 if no genre is provided', async () => {
            name = ''
            res = await exec()
            expect(res.status).toBe(400)
        })

        test('it should save if everything is valid', async () => {
            res = await exec()
            expect(res.body).toHaveProperty('id')
            expect(res.body).toHaveProperty('name', 'Romantic')
        })
    })



})