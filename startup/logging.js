const Sentry = require('@sentry/node')

module.exports = function () {
    Sentry.init({ dsn: 'https://34ae1eb8adc04dab92f7907aba6f15a3@o374406.ingest.sentry.io/5244820' })
}