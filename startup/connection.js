const { Sequelize, DataTypes } = require('sequelize')
const config = require('config')
const Sentry = require('@sentry/node')

const sequelize = new Sequelize(config.get('database'), config.get('username'), config.get('password'), {
    host: config.get('host'),
    dialect: 'mysql',
    logging: false
})

function connect() {
    sequelize.authenticate()
        .then(() => {
            require('../models/relations')
            Sentry.captureMessage('Connected successfully to the database')
            //console.log('Connected successfully to the database')

        })
        .catch((err) => {
            console.log(`Unable to connect to the database ${err}`)
            process.exit(1)
        })
}

module.exports.sequelize = sequelize
module.exports.connect = connect