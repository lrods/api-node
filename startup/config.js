const config = require('config')

module.exports = function () {
    if (!config.get('jwtPrivateKey')) {
        throw new Error('ERROR: The web token private key is not defined')
    }
}